# Description

This is an open-source replacement PCB for the Odroid-GO <https://wiki.odroid.com/odroid_go/odroid_go>

# Specifications

- 480MHz STM32H7 MCU
- 32MiB 16-bit 133MHz SDRAM (Upgradable to 64MiB, potentially overclockable to 200MHz)
- 320x240 18-bit 60Hz 2.4" LCD (Hardware dithering to 24-bit, may support 120Hz refresh)
- 12-bit Audio Output (potentially with hardware volume control)
- 4-bit microSD Card Interface (50MHz supported, potentially overclock to 100MHz)
- PWM-RGB Status LED

# Tools

You'll need KiCad 5 to view or modify the schematics/pcb.
For Ubuntu users I'd suggest using this PPA: <https://launchpad.net/~js-reynaud/+archive/ubuntu/kicad-5>

If you wish to modify the schematics then you should install the latest symbols, footprints and 3D models from git.

Routing was assisted with FreeRouting: <https://freerouting.org>


# Credits

The design for this board was concieved and implemented by Ben Brewer.

Much of the electronic design an review of the board was done by Matthias Feser;
including but not limited to:
	- Design and simulation of the safe shut-off power system
	- Design of the backlight system
	- The USB chip layout and USB flashing
	- Review of all component placement and pcb layout
	- Design of the battery charging circuitry.
